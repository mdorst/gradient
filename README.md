
# Gradient

An assignment for CSC 131 - Software Engineering at Sac State

The specifications for the project are in the `spec/` directory.

[UML Diagram](https://gitlab.com/mdorst/gradient/blob/master/spec/EngineeringDesign_Gradient.pdf)
